# streams

Simple stream library for PHP.

## Overview

This provides a few standardized interfaces for objects providing stream-like
functionality.

* `Stream`: Allow access to the underlying stream and closing a stream.
* `Readable`: Read a stream of bytes.
* `Writeable`: Write a stream of bytes.
* `Seekable`: Allow moving the pointer around a stream.

This allows for declaring precisely what your code needs, precisely what
functionality your stream provides, and allows the compiler to validate
this for us. Further, it then allows you to swap out the underlying PHP streams
for more "interesting" implementations.

## Usage

A basic PHP stream resource wrapper is provided in:

* `ReadStream`: a stream that supports reading
* `SeekableStreamStream`: a stream that supports reading and seeking
* `WriteStream`: a stream that supports writing
* `SeekableWriteStream`: a stream that supports writing and seeking

These implementations should not be type-hinted in your code. You should instead
depend on the specific interface that you require. These classes are meant to
provide the boilerplate for passing existing PHP streams where those interfaces
are used.

A few convenience methods are available:

* `ReadStream::fromPath(string $path)`: `fopen()`s a path and returns a ReadStream
  or SeekableReadStream.
* `ReadStream::fromString(string $contents)`: opens a buffer in memory, puts the
  passed string into it, and returns a ReadStream or SeekableReadStream.
* `ReadStream::fromStream($stream)`: determines whether the passed stream is
  seekable and wraps it in either a ReadStream or SeekableReadStream.
* `WriteStream::fromPath(string $path)`: `fopen()`s a path and returns a
  WriteStream or SeekableWriteStream.
* `WriteStream::fromStream($stream)`: determines whether the passed stream is
  seekable and wraps it in either a WriteStream or SeekableWriteStream.

## Interfaces

```
Stream
    unwrap(): resource
    close(): void

Readable
    read(int $bytes): string
    eof(): bool
    copy(Writeable $stream, ?int $length = null): void

Writeable
    write(string $data): void

Seekable
    offset(): int
    seek(int $offset): void
    length(): int
```

# Legal

Copyright 2024 Adam Pippin <hello@adampippin.ca>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this project except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
