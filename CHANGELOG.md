# Changelog

## v1.0.0 (2024-07-04)

* Defined contracts for streams.
* Added basic wrappers for existing PHP stream resources.
