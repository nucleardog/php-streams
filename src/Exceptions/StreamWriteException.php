<?php

namespace Nucleardog\Streams\Exceptions;

class StreamWriteException extends StreamException
{

	protected function getDefaultMessage(): string
	{
		return 'Error writing to stream';
	}

}
