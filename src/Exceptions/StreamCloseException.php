<?php

namespace Nucleardog\Streams\Exceptions;

class StreamCloseException extends StreamException
{

	protected function getDefaultMessage(): string
	{
		return 'Could not close stream';
	}

}
