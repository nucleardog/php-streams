<?php

namespace Nucleardog\Streams\Exceptions;

class StreamSeekException extends StreamException
{

	protected function getDefaultMessage(): string
	{
		return 'Error seeking in stream';
	}

}
