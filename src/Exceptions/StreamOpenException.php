<?php

namespace Nucleardog\Streams\Exceptions;

class StreamOpenException extends StreamException
{

	public function __construct(
		?string $message = null,
		?\Throwable $previous = null,
		private ?string $path = null,
	) {
		parent::__construct($message, $previous);
	}

	protected function getDefaultMessage(): string
	{
		return 'Could not open stream';
	}

	public function getPath(): ?string
	{
		return $this->path;
	}

}
