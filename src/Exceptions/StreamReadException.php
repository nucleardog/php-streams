<?php

namespace Nucleardog\Streams\Exceptions;

class StreamReadException extends StreamException
{

	protected function getDefaultMessage(): string
	{
		return 'Error reading from stream';
	}

}
