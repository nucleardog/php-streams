<?php

namespace Nucleardog\Streams\Exceptions;

class StreamException extends \Exception
{

	public function __construct(?string $message = null, ?\Throwable $previous = null)
	{
		parent::__construct(
			$message ?? $this->getDefaultMessage(),
			0,
			$previous,
		);
	}

	protected function getDefaultMessage(): string
	{
		return '';
	}

}
