<?php

declare(strict_types=1);

namespace Nucleardog\Streams;

class ReadStream extends Stream implements Contracts\Readable
{
	public static $CHUNK_SIZE = 524288; /* 524288 = 512kB */

	private $stream;

	public function __construct($stream)
	{
		parent::__construct($stream);
		$this->stream = $stream;
	}

	public static function fromPath(string $path): ReadStream
	{
		if (($stream = fopen($path, 'r')) === false) {
			throw new Exceptions\StreamOpenException(
				message: 'Could not open path for reading',
 				path: $path,
			);
		}
		return static::fromStream($stream);
	}

	public static function fromString(string $contents): SeekableReadStream
	{
		if (($stream = fopen('php://memory', 'w+')) === false) {
			throw new Exceptions\StreamOpenException(
				message: 'Could not create temporary stream',
				path: 'php://memory',
			);
		}
		if (fwrite($stream, $contents) === false) {
			throw new Exceptions\StreamWriteException();
		}
		if (fseek($stream, 0, SEEK_SET) === -1) {
			throw new Exceptions\StreamSeekException();
		}
		return static::fromStream($stream);
	}

	public static function fromStream($stream): ReadStream
	{
		$metadata = stream_get_meta_data($stream);
		if ($metadata['seekable'] === true) {
			return new SeekableReadStream($stream);
		} else {
			return new ReadStream($stream);
		}
	}

	public function eof(): bool
	{
		return feof($this->stream);
	}

	public function read(int $bytes): string
	{
		if (($chunk = fread($this->stream, $bytes)) === false) {
			throw new Exceptions\StreamReadException();
		}
		return $chunk;
	}

	public function copy(Contracts\Writeable $stream, ?int $length = null): void
	{
		for (;;) {
			if ($this->eof() || $length === 0) {
				break;
			}

			$size = static::$CHUNK_SIZE;
			if (isset($length)) {
				$size = min($length, static::$CHUNK_SIZE);
				$length -= $size;
			}
			$chunk = $this->read($size);
			$stream->write($chunk);
		}
	}

}
