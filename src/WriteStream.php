<?php

declare(strict_types=1);

namespace Nucleardog\Streams;

class WriteStream extends Stream implements Contracts\Writeable
{
	private $stream;

	public function __construct($stream)
	{
		parent::__construct($stream);
		$this->stream = $stream;
	}

	public static function fromPath(string $path): WriteStream
	{
		if (($stream = fopen($path, 'w')) === false) {
			throw new Exceptions\StreamOpenException(
				message: 'Could not open path for writing',
 				path: $path,
			);
		}
		return static::fromStream($stream);
	}

	public static function fromStream($stream): WriteStream
	{
		$metadata = stream_get_meta_data($stream);
		if ($metadata['seekable'] === true) {
			return new SeekableWriteStream($stream);
		} else {
			return new WriteStream($stream);
		}
	}

	public function write(string $data): void
	{
		if (fwrite($this->stream, $data) === false) {
			throw new Exceptions\StreamWriteException();
		}
	}

}
