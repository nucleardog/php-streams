<?php

declare(strict_types=1);

namespace Nucleardog\Streams\Concerns;

trait SeeksStreams
{

	public abstract function unwrap();

	public function offset(): int
	{
		return ftell($this->unwrap());
	}

	public function seek(int $offset): void
	{
		fseek($this->unwrap(), $offset, SEEK_SET);
	}

	public function length(): int
	{
		$oldOffset = $this->offset();
		fseek($this->unwrap(), 0, SEEK_END);
		$length = $this->offset();
		$this->seek($oldOffset);
		return $length;
	}

}