<?php

declare(strict_types=1);

namespace Nucleardog\Streams;

abstract class Stream implements Contracts\Stream
{
	private $stream;

	public function __construct($stream)
	{
		$this->stream = $stream;
	}

	public function unwrap()
	{
		return $this->stream;
	}

	public function close(): void
	{
		if (fclose($this->stream) === false) {
			throw new \Nucleardog\Streams\Exceptions\StreamCloseException();
		}
	}

}
