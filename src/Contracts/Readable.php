<?php

declare(strict_types=1);

namespace Nucleardog\Streams\Contracts;

interface Readable extends Stream
{
	/**
	 * Check if the stream is at the end
	 *
	 * @return bool
	 */
	public function eof(): bool;

	/**
	 * Read up to the specified number of bytes from the stream
	 *
	 * @param int $bytes
	 * @return string
	 */
	public function read(int $bytes): string;

	/**
	 * Copy from this stream to another
	 *
	 * @param Writeable $stream
	 * @param ?int $length copy up to this many bytes
	 */
	public function copy(Writeable $stream, ?int $length = null): void;

}