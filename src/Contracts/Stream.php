<?php

declare(strict_types=1);

namespace Nucleardog\Streams\Contracts;

interface Stream
{

	/**
	 * Return the underlying stream resource
	 *
	 * @return resource
	 */
	public function unwrap();

	/**
	 * Close the underlying stream resource
	 *
	 * @return void
	 */
	public function close(): void;

}