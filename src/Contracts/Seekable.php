<?php

declare(strict_types=1);

namespace Nucleardog\Streams\Contracts;

interface Seekable
{

	/**
	 * Get the offset within the stream
	 *
	 * @return int
	 */
	public function offset(): int;

	/**
	 * Seek to the given offset in a stream
	 *
	 * @param int $offset
	 * @return void
	 */
	public function seek(int $offset): void;

	/**
	 * Get the length of the stream
	 *
	 * @return int
	 */
	public function length(): int;

}