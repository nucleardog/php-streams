<?php

declare(strict_types=1);

namespace Nucleardog\Streams\Contracts;

interface Writeable extends Stream
{

	/**
	 * Write data to the stream
	 *
	 * @param string $data
	 * @return void
	 */
	public function write(string $data): void;

}