<?php

declare(strict_types=1);

namespace Nucleardog\Streams;

class SeekableReadStream extends ReadStream implements Contracts\Seekable
{
	use Concerns\SeeksStreams;

	private $stream;

	public function __construct($stream)
	{
		parent::__construct($stream);
		$this->stream = $stream;
	}

}